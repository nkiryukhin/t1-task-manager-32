package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IProjectService;
import ru.t1.nkiryukhin.tm.api.service.IServiceLocator;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.dto.response.*;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(
            @NotNull final ProjectClearRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse completeProjectById(
            @NotNull final ProjectCompleteByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse completeProjectByIndex(
            @NotNull final ProjectCompleteByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(
            @NotNull final ProjectCreateRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    public ProjectGetByIdResponse getProjectById(
            @NotNull final ProjectGetByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectGetByIndexResponse getProjectByIndex(
            @NotNull final ProjectGetByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectListResponse listProject(
            @NotNull final ProjectListRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(
            @NotNull final ProjectRemoveByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        getProjectService().removeById(userId, id);
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @NotNull final ProjectRemoveByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        getProjectService().removeByIndex(userId, index);
        return new ProjectRemoveByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startProjectById(
            @NotNull final ProjectStartByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startProjectByIndex(
            @NotNull final ProjectStartByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateProjectById(
            @NotNull final ProjectUpdateByIdRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse();
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @NotNull final ProjectUpdateByIndexRequest request
    ) throws AbstractException {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse();
    }

}