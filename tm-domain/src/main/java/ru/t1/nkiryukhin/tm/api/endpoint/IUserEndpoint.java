package ru.t1.nkiryukhin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.dto.response.*;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public interface IUserEndpoint {

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request) throws AbstractException;

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) throws AbstractException;

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) throws AbstractException;

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request) throws AbstractException;

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request) throws AbstractException;

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) throws AbstractException;

}
