package ru.t1.nkiryukhin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserRegistryRequest extends AbstractUserRequest {

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private String email;

}
