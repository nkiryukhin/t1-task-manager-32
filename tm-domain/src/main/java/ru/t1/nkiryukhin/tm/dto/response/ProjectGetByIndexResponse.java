package ru.t1.nkiryukhin.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Project;

@NoArgsConstructor
public final class ProjectGetByIndexResponse extends AbstractProjectResponse {

    public ProjectGetByIndexResponse(@Nullable Project project) {
        super(project);
    }

}