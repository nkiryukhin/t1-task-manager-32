package ru.t1.nkiryukhin.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import ru.t1.nkiryukhin.tm.model.User;

@Getter
@Setter
public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(User user) {
        super(user);
    }

}