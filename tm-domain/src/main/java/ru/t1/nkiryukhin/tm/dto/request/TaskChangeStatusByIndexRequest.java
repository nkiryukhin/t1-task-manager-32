package ru.t1.nkiryukhin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    @Nullable
    private Status status;

}
