package ru.t1.nkiryukhin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {

    void setSocket(@Nullable Socket socket);

}
