package ru.t1.nkiryukhin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application";
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
