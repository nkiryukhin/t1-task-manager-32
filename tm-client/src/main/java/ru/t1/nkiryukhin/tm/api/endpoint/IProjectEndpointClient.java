package ru.t1.nkiryukhin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IProjectEndpointClient extends IEndpointClient, IProjectEndpoint {

    void setSocket(@Nullable Socket socket);

}
