package ru.t1.nkiryukhin.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.nkiryukhin.tm.api.service.ICommandService;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;
import ru.t1.nkiryukhin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    protected ISystemEndpointClient getSystemEndpoint() {
        return serviceLocator.getSystemEndpointClient();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
