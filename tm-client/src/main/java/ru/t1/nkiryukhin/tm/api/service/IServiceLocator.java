package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    IDomainEndpointClient getDomainEndpointClient();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectEndpointClient getProjectEndpointClient();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

    @NotNull
    ITaskEndpointClient getTaskEndpointClient();

    @NotNull
    IUserEndpointClient getUserEndpointClient();

}
