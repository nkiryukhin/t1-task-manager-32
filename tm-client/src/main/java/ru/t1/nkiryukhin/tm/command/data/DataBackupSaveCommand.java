package ru.t1.nkiryukhin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.request.DataBackupSaveRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;


public final class DataBackupSaveCommand extends AbstractDataCommand {

    public static final String NAME = "backup-save";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save backup to file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE]");
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest();
        getDomainEndpointClient().saveDataBackup(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
