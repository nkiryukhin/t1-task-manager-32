package ru.t1.nkiryukhin.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.nkiryukhin.tm.dto.request.ApplicationAboutRequest;
import ru.t1.nkiryukhin.tm.dto.request.ApplicationVersionRequest;
import ru.t1.nkiryukhin.tm.dto.response.ApplicationAboutResponse;
import ru.t1.nkiryukhin.tm.dto.response.ApplicationVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ApplicationAboutResponse serverAboutResponse = client.getAbout(new ApplicationAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        final ApplicationVersionResponse serverVersionResponse = client.getVersion(new ApplicationVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

}
